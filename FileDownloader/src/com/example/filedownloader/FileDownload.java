package com.example.filedownloader;

import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.net.URL;
import java.net.URLConnection;

import android.util.Log;

public class FileDownload {
	
	public static final String TAG = "filedownload";
 
	public Integer DownloadFromUrl(String sourceUrl, String path, String fileName) {  //this is the downloader method
    	
    	try {
    		//Log.d(TAG, "Downloading...");
    		File outputFile = new File(path, fileName);
    		URL url = new URL(sourceUrl + "/" + fileName);
    	    URLConnection conn = url.openConnection();
    	    int contentLength = conn.getContentLength();
    	    if (contentLength < 0)
    	    {
    	    	return -2;
    	    }
    	    DataInputStream stream = new DataInputStream(url.openStream());

	        byte[] buffer = new byte[contentLength];
	        stream.readFully(buffer);
	        stream.close();
	        
	        //Log.d(TAG, "Saving...");
	        
	        DataOutputStream fos = new DataOutputStream(new FileOutputStream(outputFile));
	        fos.write(buffer);
	        fos.flush();
	        fos.close();
	        
	        //Log.d(TAG, "Downloaded and saved");
	        return 0;
	        
    	} catch(FileNotFoundException e) {
    		Log.e(TAG, e.getMessage());
            e.printStackTrace();
    	    return -2;
    	} catch (IOException e) {
    		Log.e(TAG, e.getMessage());
            e.printStackTrace();
    	    return -1;
    	}
	}
}