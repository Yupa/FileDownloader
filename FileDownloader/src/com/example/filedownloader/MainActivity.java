package com.example.filedownloader;

import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.AsyncTask;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.Toast;

public class MainActivity extends Activity {


	public static final String TAG = "mainactivity";
	private Button downloadButton, deleteButton, settingsButton;
	private String directory, site, extension;
	MainClass mainClass;

	public static Context appContext;
	SharedPreferences sharedPreferences;
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_main);
		
		appContext = getApplicationContext();
		sharedPreferences = appContext.getSharedPreferences("preferences_file", appContext.MODE_PRIVATE);
		
		mainClass = new MainClass();
		get();
        
		settingsButton = (Button) findViewById(R.id.buttonSettings);
		settingsButton.setOnClickListener(new OnClickListener() {
			
			@Override
			public void onClick(View v) {
				Intent i = new Intent(MainActivity.this, SettingsActivity.class);
				i.putExtra(SettingsActivity.SITE, site);
				i.putExtra(SettingsActivity.EXTENSION, extension);
				i.putExtra(SettingsActivity.DIRECTORY, directory);
				startActivityForResult(i, SettingsActivity.SETTINGS_ACTIVITY);
			}
		});
        
        downloadButton = (Button) findViewById(R.id.buttonDownload);
		downloadButton.setOnClickListener(new OnClickListener() {           
		
			@Override
			public void onClick(View v) {
				if (directory != null) {
					DownloadHTML pobieranie = new DownloadHTML (directory, site, extension);
			        pobieranie.execute();
				}
				else
					Toast.makeText(appContext, "Pick a folder to save to!", Toast.LENGTH_LONG).show();
			}
		});
		
		deleteButton = (Button) findViewById(R.id.buttonDelete);
		deleteButton.setOnClickListener(new OnClickListener() {    
			@Override
			public void onClick(View v) {
				delete();
			}
		});
    }
	
	@Override
    public void onResume() {
        super.onResume();
        get();
    }
	
	private void get()
	{
		//Log.d(TAG, "reading");
		site = sharedPreferences.getString("SITE", null);
		extension = sharedPreferences.getString("EXTENSION", null);
		directory = sharedPreferences.getString("DIR", null);
		String path_to_files = sharedPreferences.getString("PLIKI", null);
		//Log.d(TAG, "Path: " + path_to_files);
		
		if (path_to_files != null)
			mainClass.load(path_to_files);
	}
	
	public void updateSettings()
	{
		SharedPreferences.Editor prefEditor = sharedPreferences.edit();
		if(site != null)
			prefEditor.putString("SITE", site);
		if(extension != null)
			prefEditor.putString("EXTENSION", extension);
		if(directory != null)
			prefEditor.putString("DIR", directory);
		
		prefEditor.commit();
	}
	
	public void updatePath()
	{
		String path_to_files = mainClass.getPath();
		SharedPreferences.Editor prefEditor = sharedPreferences.edit();
		if(!path_to_files.equals(""))
			prefEditor.putString("PLIKI", path_to_files);
		else
			prefEditor.remove("PLIKI").commit();
	}
	
	private void delete()
	{
		int result = mainClass.clear();
		Toast.makeText(MainActivity.appContext, "Deleted " + result + " files.", Toast.LENGTH_SHORT).show();
		updatePath();
	}
	
	@Override
	protected void onActivityResult(int requestCode, int resultCode, Intent data) {
		if(requestCode == SettingsActivity.SETTINGS_ACTIVITY && resultCode == RESULT_OK) {
			Bundle extras = data.getExtras();
			site = (String) extras.get(SettingsActivity.SITE);
			extension = (String) extras.get(SettingsActivity.EXTENSION);
			directory = (String) extras.get(SettingsActivity.DIRECTORY);
			updateSettings();
		}
	}
	
	private class DownloadHTML extends AsyncTask<Void, Void, Integer> {

	    private String mDir, mUrl, mExtension;
	    private Document mDoc;

	    public DownloadHTML(String dir, String site, String extension) {
	        mDir = dir;
	        mUrl = site;
	        mExtension = extension;
	    }

	    @Override
	    protected void onPreExecute() {
	    	//Log.d(TAG, "onPreExecute");
	        Toast.makeText(appContext, "Starting download...", Toast.LENGTH_LONG).show();
	    }

	    @Override
	    protected Integer doInBackground(Void... params) {
	    	
	    	mDoc = null;
			try {
				mDoc = Jsoup.connect(mUrl).get();
			} 
			catch (Exception e) {
	            Log.e("TAG", e.getMessage());
	            e.printStackTrace();
	            return -1;
	        }
			Integer result = mainClass.addFiles(mDoc, mDir, mUrl, mExtension);
			return result;
	    }

	    @Override
	    protected void onPostExecute (Integer result) {
	    	if (result == -1)
	    		Toast.makeText(MainActivity.appContext, "Error. Couldn't make a connection", Toast.LENGTH_LONG).show();
	    	else
	    	{
	    		Toast.makeText(MainActivity.appContext, "Success! Downloaded " + result + " files.", Toast.LENGTH_SHORT).show();
	    		updatePath();
	    	}
	    }
	}
	
}
