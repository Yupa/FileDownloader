package com.example.filedownloader;

import java.io.File;
import java.util.ArrayList;
import java.util.List;

import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;

public class MainClass {

	//private static final String TAG = "mainClass";
	private List<String> String_List = new ArrayList<String>();
	FileDownload fileDownloader;
	
	public MainClass() {
		fileDownloader = new FileDownload();
	}
	
	public void load(String pathToFiles)
	{
		String_List.clear();
		String temporary_Strings [] = pathToFiles.split(";");
		for (int i = 0; i < temporary_Strings.length; i++)
		{
			//Log.d(TAG, "Loading file: " + temporary_Strings[i]);
			if (!temporary_Strings[i].equals(""))
				String_List.add(temporary_Strings[i]);
		}
	}
	
	public int clear()
	{
		int how_many = String_List.size();
		//Log.d(TAG, "Clear " + how_many + " strings");
		boolean result = true;
		for (String iterator : String_List) 
		{
			if(!deleteFile(iterator))
			{
				result = false; 	
				//Log.d(TAG, "Failed to delete: " + iterator);
			}
			else
			{
				//Log.d(TAG, "Successfully deleted: " + iterator);
			}
		}
		String_List.clear();
		if (!result) 
            return -1;
		else 
            return how_many;
	}

	public String getPath()
	{
		String new_path = "";
		for (String iterator : String_List )
			new_path += iterator + ";" ;
		return new_path;
	}
	
	//CHANGE HERE TO MATCH YOUR SITE'S HTML STRUCTURE
	public Integer addFiles(Document doc, String dir, String url, String extension)
	{
		int numberOfNewFiles = 0;
		for (Element file : doc.select("a")) {
			String neww = file.attr("href");
			if (!neww.startsWith(".") && neww.endsWith(extension))
			{
				String foundPath = findPathIfExist(neww);
				Integer result = fileDownloader.DownloadFromUrl(url, dir, neww);
				if (result == 0)
				{
					//Log.d(TAG, "Successfully downloaded: " + new);
					numberOfNewFiles++;
					if (foundPath == null) {
						String path_to_new_file = dir + "/" + neww;
						String_List.add(path_to_new_file);
					}
				}
				else
				{
					//Log.d(TAG, "Failed to download: " + new);
				}
					
			}
		}
		return numberOfNewFiles;
	}
	
	private String findPathIfExist (String fileName)
	{
		for (String iterator : String_List )
		{
			if (iterator.endsWith(fileName))
				return iterator;
		}
		return null;
	}
	
	private boolean deleteFile(String pathToFile)
	{
		//Log.d(TAG, "Deleting: " + pathToFile);
		File file = new File(pathToFile);
		boolean deleted = file.delete();
		return deleted;
	}
}
