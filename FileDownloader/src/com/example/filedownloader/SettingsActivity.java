package com.example.filedownloader;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.KeyEvent;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

public class SettingsActivity extends Activity {

	public static final int SETTINGS_ACTIVITY = 16549543;
	public static final String SITE = "site";
	public static final String DIRECTORY = "directory";
	public static final String EXTENSION = "extension";
	public static final String TAG = "settingsctivity";
	
	private Button folderButton, saveButton, cancelButton;
	private TextView textViewDir;
	private String site, extension, dir;

	private EditText editTextSite, editTextExtension;
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_settings);
		
		Bundle b = getIntent().getExtras();
		site = b.getString(SettingsActivity.SITE);
		extension = b.getString(SettingsActivity.EXTENSION);
		dir = b.getString(SettingsActivity.DIRECTORY);
		
		textViewDir = (TextView) findViewById(R.id.textViewFolderDir);
		editTextSite= (EditText)findViewById(R.id.editTextSite);
		editTextExtension = (EditText)findViewById(R.id.editTextExtension);
		
		saveButton = (Button) findViewById(R.id.buttonSave);
		saveButton.setOnClickListener(new OnClickListener() {           
			
			@Override
			public void onClick(View v) {
				quit();
			}
		});
		
		cancelButton = (Button) findViewById(R.id.buttonCancel);
		cancelButton.setOnClickListener(new OnClickListener() {           
			
			@Override
			public void onClick(View v) {
				finish();
			}
		});
		
		folderButton = (Button) findViewById(R.id.buttonChoose);
        folderButton.setOnClickListener(new OnClickListener() 
        {
        	@Override
            public void onClick(View v) 
            {
        		save();
        		Intent intent = new Intent(SettingsActivity.this, DirectoryPicker.class);
        		startActivityForResult(intent, DirectoryPicker.PICK_DIRECTORY);
            }
        });
	}
	
	private void quit()
	{
		save();
		Intent result = new Intent();
    	result.putExtra(SettingsActivity.SITE, site);
    	result.putExtra(SettingsActivity.EXTENSION, extension);
    	result.putExtra(SettingsActivity.DIRECTORY, dir);
        setResult(RESULT_OK, result);
        finish();
	}
	
	private void get()
	{
		if (site != null)
			editTextSite.setText(site);
		if (extension != null)
			editTextExtension.setText(extension);
		if (dir != null)
			textViewDir.setText(dir);
	}
	
	private void save()
	{
		site  = editTextSite.getText().toString();
		extension = editTextExtension.getText().toString();
	}
	
	@Override
    public void onResume() {
        super.onResume();
        get();
    }

	//AFTER CHOOSING DIRECTORY
	@Override
	protected void onActivityResult(int requestCode, int resultCode, Intent data) {
		if(requestCode == DirectoryPicker.PICK_DIRECTORY && resultCode == RESULT_OK) {
			Bundle extras = data.getExtras();
			dir = (String) extras.get(DirectoryPicker.CHOSEN_DIRECTORY);
			textViewDir.setText(dir);
			save();
		}
	}
	
	@Override
	public boolean onKeyDown(int keyCode, KeyEvent event)  {
	    if (keyCode == KeyEvent.KEYCODE_BACK && event.getRepeatCount() == 0) {
	        finish();
	    }
	    return super.onKeyDown(keyCode, event);
	}
}
